import java.io.FileInputSteam;
import PersonPackage.Person;

public class ReadClass {
  public void main(String[] args) {
    FileInputSteam fis = new FileInputSteam("persons.txt");
    ObjectInputStream ois = new ObjectInputStream(fis);

    Person p = (Person) ois.readObject();
    ois.close();

    p.reviewPerson();
  }
}