public class Main {
  public static void main(String[] args) {
    int number = 26;
    char[] str = "00000000000000000000000000000000".toCharArray();
    int base = 2;
    int j = 0;
    int c = 0;

    for(int i = 0; i <= number; i++) {
      j++;
      str[c] = (char)j;

      if(j >= base - 1) {
        j = 0;
        c++;
      }
    }

    System.out.println(String.valueOf(str));
  }
}